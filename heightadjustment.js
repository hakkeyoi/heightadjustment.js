(function(factory){

    window.HeightAdjustment = factory();

})(function(){
    'use strict';

    var HeightAdjustment = function(targetElement,options){

        // generate obj
        var heightAdjustment = {};

        if(typeof targetElement === 'string'){
            heightAdjustment.targetEl = document.querySelectorAll(targetElement);
        }

        if(heightAdjustment.targetEl.length === 0){
            throw new Error('targetElement not found.');
        }

        heightAdjustment.column = (typeof options !== 'undefined' && typeof options.column ==='number') ? options.column : null; 

        var init = function(){

            // obj to array
            var list = Array.prototype.slice.call(heightAdjustment.targetEl);

            // reset inline style
            list.forEach(function(v){
                v.removeAttribute('style');
            });

            if(heightAdjustment.column === null){
                adjust(list);
            }else{
                var num = Math.ceil(list.length / heightAdjustment.column);
                var amount = heightAdjustment.column;
                for (var i = 0; i < num; i++){
                    var splitList = list.splice(0,amount);
                    adjust(splitList);
                };
            };

        };

        var adjust = function(adjustList){

            // get each target height
            var heightList = [];
            adjustList.forEach(function(v){
                heightList.push(v.offsetHeight);
            });

            // get maximum height
            var maxHeight = heightList.reduce(function(r,v){
                return v > r ? v : r;
            },0);

            // set each target style
            adjustList.forEach(function(v){
                v.setAttribute('style','height:' + maxHeight + 'px;');
            });
        };

        // set window event
        window.addEventListener('load',init,false);
        window.addEventListener('resize',init,false);

        return heightAdjustment
    };

    return HeightAdjustment;
});